<?php

/*
 * @category  Projects
 * @package   self.socialFella.reborn
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2015 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */

/**
 * Description of EPheanstalk
 *
 * @author vladislav
 */

require_once dirname(__FILE__).'/../vendor/autoload.php';

use Pheanstalk\Pheanstalk;
use Pheanstalk\PheanstalkInterface;
use YiiDelegation\components\DelegatingComponent;

class EPheanstalk extends DelegatingComponent {
    
    public $host = '127.0.0.1';
    public $port, $connectTimeout;
    
    public function init() {
    
        $this->port = is_null($this->port) ? PheanstalkInterface::DEFAULT_PORT : $this->port;
        $this->delegate = new Pheanstalk($this->host, $this->port, $this->connectTimeout);
        parent::init();
    }
    
}
